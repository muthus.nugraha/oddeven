package handlers

import (
	"assesment/app/resources"
	"fmt"
)

func GetData(index int) {
	biodatas := []*resources.Biodata{
		{Name: "Bayu", Address: "Jakarta", Jobs: "programmer", Reason: "Idem"},
		{Name: "Hilmi", Address: "Jakarta", Jobs: "programmer", Reason: "Idem"},
		{Name: "Eka", Address: "Serpong", Jobs: "programmer", Reason: "Idem"},
		{Name: "Agus", Address: "Bogor", Jobs: "programmer", Reason: "Idem"},
		{Name: "Yudha", Address: "Jakarta", Jobs: "programmer", Reason: "Idem"},
		{Name: "Fatur", Address: "Jakarta", Jobs: "programmer", Reason: "Idem"},
		{Name: "Aditya", Address: "Jakarta", Jobs: "programmer", Reason: "Idem"},
		{Name: "Clara", Address: "Depok", Jobs: "programmer", Reason: "Idem"},
		{Name: "Medy", Address: "Jakarta", Jobs: "programmer", Reason: "Idem"}}

	if index >= 0 && index < len(biodatas) {
		println("Nama :", biodatas[index].Name, ", Address :", biodatas[index].Address, ", Jobs :", biodatas[index].Jobs, ", Reason :", biodatas[index].Reason)
	} else {
		println("Absent number not found!")
	}
}

func StructFriend() {
	persons := []*resources.Friend{
		{Name: "Bayu"},
		{Name: "Hilmi"},
		{Name: "Eka"},
		{Name: "Agus"},
		{Name: "Yudha"},
		{Name: "Fatur"},
		{Name: "Aditya"},
		{Name: "Clara"},
		{Name: "Medy"}}

	printFriend := func(friends []*resources.Friend) {
		for _, friend := range friends {
			fmt.Println(friend.Name)
		}
	}

	printFriend(persons)
}

func PrintFriends() {
	var friends = []string{"Bayu",
		"Hilmi", "Eka", "Satrio",
		"Agus", "Yudha", "Fatur", "Aditya", "Clara", "Medy"}
	for index := range friends {
		fmt.Println(index+1, friends[index])
	}
}

func Deret() {
	for i := 0; i < 10; i++ {
		if (i+1)%2 == 0 {
			fmt.Println("", i+1, " Genap")
		} else {
			fmt.Println(i+1, " Ganjil")
		}
	}
}
