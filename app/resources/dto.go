package resources

type Biodata struct {
	Name    string
	Address string
	Jobs    string
	Reason  string
}

type User struct {
	Name     string
	Username string
}

type Friend struct{ Name string }

type Employee struct {
	ID       int
	Name     string `json:name`
	Age      int
	Division string
}

var emps = []Employee{
	{
		ID:       1,
		Name:     "Muthus",
		Age:      24,
		Division: "IT",
	},
}

func NextIDEmployee() int {
	return len(emps) + 1
}

func GetEmployees() []Employee {
	return emps
}

func CreateEmployee(emp *Employee) []Employee {
	emps = append(emps, *emp)
	return emps
}
