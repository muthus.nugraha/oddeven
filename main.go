package main

import (
	"assesment/app/handlers"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"time"
)

func main() {
	// var port = ":9000"
	// employees.Init()
	// http.ListenAndServe(port, nil)
	runningKorekApi()
}

type userSinging struct {
	time      int
	endPlayer string
}

func runningKorekApi() {
	ch := make(chan *userSinging)
	gameend := make(chan *userSinging)

	min := 10
	max := 30
	var breakPoint = rand.Intn(max-min) + min
	go runningGame(breakPoint, "Ismail", ch, gameend)
	go runningGame(breakPoint, "Bayu", ch, gameend)
	go runningGame(breakPoint, "Hilmi", ch, gameend)
	go runningGame(breakPoint, "Kevin", ch, gameend)

	removeData(ch, gameend)

	close(ch)
	close(gameend)
}

func runningGame(breakPoint int, name string, ch, gameend chan *userSinging) {

	rand.Seed(time.Now().UnixNano())
	min := 1
	max := 200
	for {
		select {
		case data := <-ch:
			v := rand.Intn(max-min) + min
			time.Sleep(500 * time.Millisecond)

			data.time++
			data.endPlayer = name
			if v == breakPoint || v%breakPoint == 0 {
				fmt.Println("Korek berhenti di", name)
				gameend <- data
				return
			}
			ch <- data

			fmt.Println("Korek di ", name, "saat hit ke", data.time, " dengan nilai ", v)
		}
	}
}

func removeData(c, gameend chan *userSinging) {
	c <- new(userSinging)
	for {
		select {
		case d := <-gameend:
			fmt.Println(d.endPlayer, "Kalah")
			return
		}
	}
}

func processData() {
	data := os.Args
	index := data[1]
	i, err := strconv.Atoi(index)
	if err != nil {
		fmt.Println("Sorry, your input is not number!")
	} else {
		handlers.GetData(i - 1)
	}
}
